import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
currentUser;
  constructor(private http: HttpClient, private router: Router) { 
    
    if(localStorage.getItem('token')){
      this.currentUser = jwt_decode(localStorage.getItem('token')).data;
    }
    else this.currentUser = '';
  }
  async register(user){
    await this.http.post("http://localhost:3000/users/register",user).subscribe(data=>{
      this.router.navigateByUrl('login')
    })
  }
  login(user){
    return this.http.post("http://localhost:3000/users/login",user);
  }
  isLoggedIN(){
    if (this.currentUser!==''){
      return this.isTokenValid();
    }
    else{
      return false
    }
  }
  isTokenValid(){
    const decodedToken = jwt_decode(localStorage.getItem('token'));
    const date = new Date(); 
    date.setUTCSeconds(decodedToken.exp);
    return (date.valueOf() > new Date().valueOf());
  }
  checkRole(){
    if (this.currentUser!==''){
    if (this.currentUser.role=='admin')
    return 'admin'
    else{
      return 'notadmin'
    }
  }else 
  return 'notadmin'
  }
}
