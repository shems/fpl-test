import { Component, OnInit } from '@angular/core';
import {Router } from '@angular/router';
import { AlertService } from '../../_alert/alert.service';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import * as jwt_decode from 'jwt-decode';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private router: Router, private alert: AlertService, private uService: UsersService) { }

  ngOnInit() {
    this.loginForm= new FormGroup({
      username: new FormControl('',[Validators.required]),
      pwd: new FormControl('',[Validators.required])
    })
  }

  onSubmit(){
    this.uService.login(this.loginForm.value).subscribe(data=>{
      if(data['accessToken']){
        localStorage.setItem('token', data['accessToken']);
        this.uService.currentUser= jwt_decode(localStorage.getItem('token')).data;
        this.router.navigateByUrl('/sujets');
      }
      else{
        this.alert.error(data['message'], {'autoClose':true});
      }
    });
  }

}
