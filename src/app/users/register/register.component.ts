import { Component, OnInit } from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import { UsersService } from '../users.service';
import {Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router, private uService: UsersService) { }

  RegisterForm: FormGroup;

  ngOnInit() {
    this.RegisterForm= new FormGroup({
      username: new FormControl('',[Validators.required]),
      email: new FormControl('',[Validators.required, Validators.email]),
      pwd: new FormControl('',[Validators.required]),
      cpwd: new FormControl('',[Validators.required]),
      terms: new FormControl(false,[Validators.required])
    })
  }

  onSubmit(){
    if(this.RegisterForm.value.terms){
      delete this.RegisterForm.value.terms;
    delete this.RegisterForm.value.cpwd;
    this.uService.register(this.RegisterForm.value);
    }
    
  }


}
