import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material/material.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AlertModule } from './_alert';
import {UsersModule} from './users/users.module';
import {SujetsModule} from './sujets/sujets.module';
import { LoginComponent} from './users/login/login.component';
import { RegisterComponent} from './users/register/register.component';
import { ListComponent} from './sujets/list/list.component';
import { AddComponent} from './sujets/add/add.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';

// import{ UsersComponent} from './users/users.component';
// import{ SujetsComponent} from './sujets/sujets.component';

// const appRoutes: Routes = [
//   { path: '', redirectTo: 'users', pathMatch: 'full'},
//   { path: 'users',loadChildren:'./users-management/users.module#UsersModule'},
//   { path: 'sujets', loadChildren:()=> import ('./sujets/sujets.module').then(m => m.SujetsModule)}
  
// ];

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'sujets', component: ListComponent},
  { path: 'registration', component: RegisterComponent},
  { path: 'add', component: AddComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full'}
];


@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ListComponent,
    AddComponent,
    AppComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    UsersModule,
    ReactiveFormsModule,
    FormsModule,
    SujetsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    AlertModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }