import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SujetsService {

  constructor(private http: HttpClient, private router: Router) {}
    
   
    async getSujets(){
      return await this.http.get("http://localhost:3000/sujets/");
      
    }
    voteYes(id){
      return this.http.get("http://localhost:3000/sujets/voteyes");
    }
    voteNo(id){
      return this.http.get("http://localhost:3000/sujets/voteno");
    }
}
