import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SujetsService } from '../sujets.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
sujets;
  constructor(private router: Router, private sService: SujetsService) { }

  ngOnInit() {
    // this.sService.getSujets().subscribe(data => {
     let oui;
    //   this.sujets= data
    //   console.log('these are the sujets ', this.sujets);
    this.sujets = this.sService.getSujets();
      
      for (let sujet of this.sujets){
        for (let vote of sujet.votes){
          if (vote == "Oui")
          oui = oui+1
        }
        sujet['percentage']= oui/ sujet.votes.legnth * 100
         
          
      }
        
  }

  voteYes(id){
    this.sService.voteYes(id);
  }
  voteNo(id){
    this.sService.voteNo(id);
  }

}
