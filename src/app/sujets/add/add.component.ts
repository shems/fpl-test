import { Component, OnInit } from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  sujetForm: FormGroup;
  constructor() { }

  ngOnInit() {
    this.sujetForm= new FormGroup({
      username: new FormControl('',[Validators.required]),
      pwd: new FormControl('',[Validators.required])
    })
  }

  onSubmit(){
    
  }

}
